class RPNCalculator
  def initialize
    @stack = []
  end

  def push(num)
    @stack.push(num)
  end

  def plus
    popped = @stack.pop(2)
    raise "calculator is empty" if popped.length < 2
    @stack.push(popped.reduce(:+)).last
  end

  def minus
    popped = @stack.pop(2)
    raise "calculator is empty" if popped.length < 2
    @stack.push(popped.reduce(:-)).last
  end

  def divide
    popped = @stack.pop(2)
    raise "calculator is empty" if popped.length < 2
    reduced_popped = popped.reduce do |acc, el|
      acc.to_f / el.to_f
    end
    @stack.push(reduced_popped).last
  end

  def times
    popped = @stack.pop(2)
    raise "calculator is empty" if popped.length < 2
    @stack.push(popped.reduce(:*)).last
  end

  def value
    @stack.last
  end

  def tokens(string)
    string.split.map do |el|
      if "+-/*".include?(el)
        el.to_sym
      else
        el.to_i
      end
    end
  end

  def evaluate(string)
    tokens(string).each do |el|
      if el.class == Fixnum
        push(el)
      elsif el == :+
        plus
      elsif el == :-
        minus
      elsif el == :/
        divide
      elsif el == :*
        times
      end
    end
    value
  end

end
